# Discord-Compare

A tool to compare the user base of different discord servers against one reference server.
Work with Discord-History-Tracker text dumps: https://github.com/chylex/Discord-History-Tracker


# Usage
```
    ./compare.py dht-reference.txt *dht.txt
    ex : ./compare.py dht-reference.txt dht-server1.txt dht-server2.txt
```
