#! /usr/bin/env python3

import json
import sys
import getopt


def usage():
    print("""Compare users on multiple discord dump.
Work with Discord-History-Tracker text dumps: https://github.com/chylex/Discord-History-Tracker
Usage:
    ./""", sys.argv[0], """dht-reference.txt *dht.txt
    ex : ./""", sys.argv[0], """dht-reference.txt dht-server1.txt dht-server2.txt""")
    exit(2)


def main(argv):
    try:
        opts, args = getopt.getopt(argv, "h")
    except getopt.GetoptError:
        usage()
    if len(opts) or len(args) < 2:
        usage()

    ref_members = {}

    for file in args:
        try:
            with open(file) as f:
                data = json.load(f)
        except FileNotFoundError:
            print('File ', args[0], " not found.")
            exit()
        if ref_members == {}:
            ref_members = data['meta']['users']
        else:
            for m in data['meta']['users']:
                if m in ref_members:
                    print(m, ': ', ref_members[m])


if __name__ == "__main__":
    main(sys.argv[1:])